import React, { Component } from 'react';
import { Spin } from 'antd';
import ListBeer from '../components/beer/list';

class BeerPage extends Component {
  state = {
    items: []
  };
  // onItemBeerClick = item => {
  //   console.log(item);

  //   this.setState({
  //     visible: true,
  //   });
  // }
  componentDidMount() {
    fetch('https://api.punkapi.com/v2/beers?page=1&per_page=40')
      .then(response => response.json())
      .then(items => this.setState({ items }));
  }

  render() {
    console.log(this.onItemBeerClick);

    return (
      <div>
        {this.state.items.length > 0 ? (
          <ListBeer
            items={this.state.items}
            onItemBeerClick={this.props.onItemBeerClick}
          />
        ) : (
          <Spin size="large" />
        )}
      </div>
    );
  }
}

export default BeerPage;
